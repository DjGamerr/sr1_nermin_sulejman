//Klasa u kojoj drzimo informacije iz tabele stops, kao i CSV fajla
public class stops {	
	public int stop_id;
	public int stop_code;
	public int zone_id;
	public int parent_station;
	public String stop_name;
	public String stop_desc;
	public String stop_url;
	public String location_type;
	public float stop_lat;
	public float stop_lon;

}
