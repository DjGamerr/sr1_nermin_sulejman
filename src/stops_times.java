//Klasa u kojoj drzimo informacije iz tabele stop_times, kao i CSV fajla
public class stops_times {
	public String trip_id;
	public String arrival_time;
	public String departure_time;
	public int stop_id;
	public int stop_sequence;
	public String stop_headsign;
	public String pickup_type;
	public String drop_off_type;
	public String shape_dist_traveled;
}
