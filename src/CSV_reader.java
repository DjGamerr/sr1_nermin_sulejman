import java.io.FileReader;
import java.util.*;
import java.io.*;

public class CSV_reader {
	
	public static List<stops> readCSVstops(String filepath){	
		//Kreiramo listu u koju ce biti sacuvane svi procitani objekti
		List<stops> out = new ArrayList<stops>();		
		try{
			//Kreiramo novi input stream koji pokazuje na fajl iz resurca "res"
			InputStream rdr =CSV_reader.class.getResourceAsStream(filepath);
			//te kreiramo novi scanner od input streama sa encoding-om UTF-8
			Scanner fstream = new Scanner(rdr,"UTF-8");
			while(true){
				//privremena varijabla koja cuva novoprocitani red iz fajla
				String tmp = fstream.nextLine();
				//ako je procitani red prazan ili ga nikako nema onda prekini daljnje citanje.
				if(tmp == null || tmp.isEmpty()==true){break;}
				//u privremeni array spremamo razdvojenu varijablu tmp po ","
				//napomena -1 znaci samo jedan prelaz tako da nam ostanu i prazni dijelovi u fajlu
				String[] arr = tmp.split(",",-1);
				//Kreiramo novi objekat (stops) koji ce sluziti za unos dobivenih informacija
				stops finalvar = new stops();
				//dobivene informacije ubacujemo u stops
				finalvar.stop_id=tryParseInt(arr[0]);
				finalvar.stop_code=tryParseInt(arr[1]);
				finalvar.zone_id=tryParseInt(arr[6]);
				finalvar.parent_station=tryParseInt(arr[9]);
				finalvar.stop_name=arr[2];
				finalvar.stop_desc=arr[3];
				finalvar.stop_url=arr[7];
				finalvar.location_type=arr[8];
				finalvar.stop_lat=tryParseFloat(arr[4]);
				finalvar.stop_lon=tryParseFloat(arr[5]);
				
				//Ako su stringovi prazni obavezno ih postaviti na "NA"
				
				if(finalvar.stop_desc==null || finalvar.stop_desc.equals("")){finalvar.stop_desc="NA";}
				if(finalvar.stop_url==null || finalvar.stop_url.equals("")){finalvar.stop_url="NA";}
				if(finalvar.location_type==null || finalvar.location_type.equals("")){finalvar.location_type="NA";}
				
				//Rezultat dodajemo u out
				out.add(finalvar);
			}
			//Zatvaramo citace
			rdr.close();
			fstream.close();
		}catch(Exception ex){System.out.println(ex.toString());}	
		//Te listu vracamo nazad
		return out;
	}
	
	public static List<stops_times> readCSVstops_times(String filepath){
		//Isto kao i gore, samo za stop_times
		List<stops_times> out = new ArrayList<stops_times>();
		try{
			InputStream rdr = CSV_reader.class.getResourceAsStream(filepath);
			Scanner fstream = new Scanner(rdr,"UTF-8");
			while(true){
				String tmp = fstream.nextLine();
				if(tmp==null || tmp.isEmpty()==true) break;
				String[] arr = tmp.split(",",-1);
				stops_times finalvar = new stops_times();
				finalvar.trip_id=arr[0];
				finalvar.arrival_time=arr[1];
				finalvar.departure_time=arr[2];
				finalvar.stop_id=tryParseInt(arr[3]);
				finalvar.stop_sequence=tryParseInt(arr[4]);
				finalvar.stop_headsign=arr[5];
				finalvar.pickup_type=arr[6];
				finalvar.drop_off_type=arr[7];
				finalvar.shape_dist_traveled=arr[8];
				if(finalvar.trip_id==null || finalvar.trip_id.equals("")) finalvar.trip_id="NA";
				if(finalvar.arrival_time==null || finalvar.arrival_time.equals("")) finalvar.arrival_time="NA";
				if(finalvar.departure_time==null || finalvar.departure_time.equals("")) finalvar.departure_time="NA";
				if(finalvar.stop_headsign==null || finalvar.stop_headsign.equals("")) finalvar.stop_headsign="NA";
				if(finalvar.pickup_type==null || finalvar.pickup_type.equals("")) finalvar.pickup_type="NA";
				if(finalvar.drop_off_type==null || finalvar.drop_off_type.equals("")) finalvar.drop_off_type="NA";
				if(finalvar.shape_dist_traveled==null || finalvar.shape_dist_traveled.equals("")) finalvar.shape_dist_traveled="NA";
				out.add(finalvar);
			}
			rdr.close();
			fstream.close();
		}catch(Exception ex){System.out.println(ex.toString());}
		return out;
	}
	
	static int tryParseInt(String value) { 
		//dodatna funkcija, ako je broj prazan onda vrati 0
		 try {  
		     return Integer.parseInt(value);  
		  } catch(NumberFormatException nfe) { 
		      return 0;
		  }  
		}
	static float tryParseFloat(String value) { 
		//isto kao i gore samo za float
		 try {  
		     return Float.parseFloat(value);  
		  } catch(NumberFormatException nfe) {  
		      return 0.0f;
		  }  
		}
	
}
