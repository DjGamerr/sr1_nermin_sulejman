import java.util.*;

import javax.swing.JOptionPane;
import javax.swing.plaf.synth.Region;

import java.beans.Statement;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.Effect;
import javafx.scene.control.Tab;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
@SuppressWarnings("unused")
public class MainClass extends Application {
    private static List<stops> stopsCSV;
    private static List<stops_times> stop_timesCSV;
	StackPane mainscreen = new StackPane();
	TabPane mainTabPane = new TabPane();	
    
    
	public static void main(String[] args) {
		//Konektujemo se na databazu 
		DBTools.tryConnect();	
		
		//Kreiramo privremenu listu koja ce biti zaduzena da iz fajla stops prebaci informacije u bazu
		List<Object> tmplist1 = new ArrayList<Object>();
		//Pozivamo csv_reader da procita fajl iz "res" foldera
		tmplist1.addAll(CSV_reader.readCSVstops("res/stops.txt"));
		//Punimo tabelu "stops" sa kreiranom listom
		DBTools.insertIntoTable("stops",tmplist1);	
		//Iz baze uzimamo infomacije u trajnu listu
		stopsCSV = DBTools.selectFromStops();
		
		
		//Isto kao i gore samo za stop_times
		List<Object> tmplist2 = new ArrayList<Object>();
		tmplist2.addAll(CSV_reader.readCSVstops_times("res/stop_times.txt"));
		DBTools.insertIntoTable("stop_times", tmplist2);
		stop_timesCSV=DBTools.selectFromStop_times();
		
		launch(args);
	}

	@Override
	public void start(Stage curscene) throws Exception {	
		//Postavljamo ime prozoru
		curscene.setTitle("Seminarski rad");
		//Prozor ne smije biti manji od 500px sirine
		curscene.setMinWidth(500);
		//Kreiramo glavni tab sa imenom "Stops"
		Tab home = new Tab("Stops");
		
		//Kreiramo listview koja ce imati informacije iz tabele stops
		ListView<String> mainview = new ListView<String>();		
		//te je punimo sa informacijama
		for(stops item : stopsCSV){mainview.getItems().add(item.stop_id + ", " + item.stop_name);}
		
		//Dodajemo dva gumba sa pripadajucim imenima
		Button mainbutton1 = new Button("Prikazi vremena");
		Button mainbutton2 = new Button("Izracunaj udaljenost");
		
		//Sve kreirane kontrole ubacujemo u StackPane
		mainscreen.getChildren().add(mainview);
		mainscreen.getChildren().add(mainbutton1);
		mainscreen.getChildren().add(mainbutton2);
		//Te ih orjentisemo
		StackPane.setAlignment(mainbutton1,Pos.BOTTOM_CENTER);
		StackPane.setAlignment(mainbutton2, Pos.BOTTOM_RIGHT);
		//Stackpane ubacujemo u home Tab
		home.setContent(mainscreen);
		//glavni tab se ne moze zatvoriti
		home.setClosable(false);
		//popunjeni tab ubacujemo u TabPane
		mainTabPane.getTabs().add(home);
		
		mainbutton1.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				//Ako nista sa liste nije odabrano onda izbaci poruku
				if(mainview.getSelectionModel().getSelectedItems().size()<1){
					MsgBox("Izbor ne moze biti prazan","Upozorenje");
				} else{
					//u protivnom se kreira privremena lista
					List<stops_times> result = new ArrayList<stops_times>();
					//Od odabranog elementa sa liste nam je potreban id, te dijelimo izbor po ","
					String[] tmpslit = mainview.getSelectionModel().getSelectedItem().toString().split(",");
					//te ga ubacujemo u filter
					int filter = CSV_reader.tryParseInt(tmpslit[0]);
					//ako u stop_times postoji dati ID onda ga prebacujemo u gore kreiranu listu
					for (stops_times item : stop_timesCSV){
						if(item.stop_id==filter) result.add(item);
					}
					//Kreiramo novi tab
					Tab tmp = new Tab();
					//Ime taba ce biti ime grada iz stops
					String[] tmpname = mainview.getSelectionModel().getSelectedItem().toString().split(",");
					//Dok ce u zagradi pisati koliko elemenata ovaj tab zadrzi
					tmp.setText(tmpname[1] + "(" + result.size() + ")");
					//Kreiramo ListView i sve informacije iz liste prebacujemo u nju
					ListView<String> secview = new ListView<String>();
					for(stops_times item : result) secview.getItems().add(item.stop_id + "," + item.trip_id + "," + item.departure_time + "," + item.arrival_time);
					//Listview ubacujemo u tab
					tmp.setContent(secview);
					//Te novokreirani tab ubacujemo u TabPane
					mainTabPane.getTabs().add(tmp);
				}
			}
			
		});
		
		mainbutton2.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			//Ako nista sa liste nije odabrano izbaci poruku
			public void handle(ActionEvent arg0) {
				if(mainview.getSelectionModel().getSelectedItems().size()<1){
					MsgBox("Izbor ne moze biti prazan","Upozorenje");
				}else{
					//U protivnom kreiraj novi TextInputDialog
					TextInputDialog dg = new TextInputDialog("Unos");
					//Postavljamo mu header text i label text
					dg.setHeaderText("Unos");
					dg.setContentText("Unesite zeljeni radius (u KM)");
					//varijabla zaduzena za vracanje unosa te se poziva dialog
					Optional<String> res = dg.showAndWait();
					//U slucaju da postoji povratna informacija
					if(res.isPresent()){
						//kreiramo output varijablu tipa float koja ce drzati unos radiusa
						float outp = CSV_reader.tryParseFloat(res.get().toString());
						//ako je manji ili jednak nuli onda radius neispravan
						if(outp<=0) {MsgBox("Unos je neispravan!","Upozorenje");} else{
							//Uzimamo izabrani item iz listview-a te ga splitamo
							String[] tmpsplit = mainview.getSelectionModel().getSelectedItem().split(",");
							//varijabla koja je zaduzena za cuvanje trenutnog izbora 
							stops curitem = new stops();
							//Kreiramo listview gdje ce biti spremljene sve povratne informacije
							ListView<String> result = new ListView<String>();
							//pomocu izabranog objekta sa listview-a pronalazimo trenutno izabrani objekat iz liste te ga spremamo u curitem
							for(stops item : stopsCSV)
								if(item.stop_id==CSV_reader.tryParseInt(tmpsplit[0]))
									curitem=item;
							//opet prolazimo kroz istu listu i u slucaju da objekat nije curitem
							for(stops item : stopsCSV){
								if(item!=curitem){
									//onda kreiramo rez varijablu koja ce cuvat racunanje iz klase DistanceCalculator
									double rez = DistanceCalculator.distance(curitem.stop_lat, curitem.stop_lon, item.stop_lat, item.stop_lon,"K" );
									//i ako je rez manja ili jednaka unosu onda u listview ubaci taj objekat prema datom sablonu
									if(rez<=outp) result.getItems().add(item.stop_id + "," + item.stop_name + "," + DistanceCalculator.roundTwoDecimals(rez) + "KM");								
								}
							}
							//Na kraju kreiramo novi tab
							Tab thtab = new Tab();
							//Dajemo mu pripadajuci text i kontrolu koju ce drzati u sebi
							thtab.setText(tmpsplit[1] + "," + outp + "KM(" + result.getItems().size() + ")");
							thtab.setContent(result);
							//te ga ubacujemo u tabpane
							mainTabPane.getTabs().add(thtab);	
						}
					}
				}
				
			}
			
		});
		
		//Postavljamo rezoluciju prozora na 400x500 s tim da je glavna scena mainTabPane(TabPane)
		curscene.setScene(new Scene(mainTabPane,400,500));
		//te je prikazujemo
		curscene.show();
		
	}

    public static void MsgBox(String infoMessage, String titleBar)
    {
    	//Preuzeto sa interneta, prikazuje alert poruku.
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
	
}
