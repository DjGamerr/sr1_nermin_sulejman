import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBTools {
    private static String dbURL = "jdbc:derby://localhost:1527/seminarski;";
    private static Connection conn = null;
    
    public static void tryConnect(){
    	//Pokusavanje povezivanja na bazu, i u slucaju da ne uspije prekinuti rad aplikacije.
    	try{
    		Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
    		conn=DriverManager.getConnection(dbURL);
    	}catch(Exception ex){System.out.println(ex.toString()); System.exit(1);}
    	//Obrisi sve zapise iz tabela "stops" i "stop_times"
    	try{java.sql.Statement stm = conn.createStatement(); stm.executeUpdate("delete from stop_times where 1=1");} catch(Exception ex){System.out.println(ex.toString());}
    	try{java.sql.Statement stm = conn.createStatement(); stm.executeUpdate("delete from stops where 1=1");} catch(Exception ex){System.out.println(ex.toString());}
    	//Radi lakseg citanja, autocommit je false, pa cemo kasnije rucno zapisivati promjene.
    	try{conn.setAutoCommit(false);}catch(Exception ex){System.out.println(ex.toString());}
    }
    
    public static void insertIntoTable(String Tablename,List<Object> items){
    	if(Tablename.equals("stops")){
    		//Listamo kroz dobivenu listu
    		for(Object curitem : items){
    			//te je castamo u stops
    			stops tmp = (stops)curitem;
    			//kreiramo sql commandu tipa
    			//insert into stops values(...)
    			String sqlcmd="insert into stops values(" + tmp.stop_id + "," + tmp.stop_code + "," + tmp.zone_id + "," + tmp.parent_station + ",'" + tmp.stop_name + "','" + tmp.stop_desc + "','" + tmp.stop_url + "','" + tmp.location_type + "'," + tmp.stop_lat + "," + tmp.stop_lon + ")"; 
    			try{
    				//kreiramo novi statement
    				java.sql.Statement tmpstat = conn.createStatement();
    				//te pokrecemo nas query
    				tmpstat.execute(sqlcmd);
    				//zapisujemo promjene u tabeli
    				conn.commit();
    				//zatvaramo statement
    				tmpstat.close();
    			}catch(Exception ex){System.out.println(ex.toString());}
    		}
    	}
    	if(Tablename.equals("stop_times")){
    		//isto kao i gore samo za stop_times
    		for(Object curitem : items){
    			stops_times tmp = (stops_times)curitem;
    			String sqlcmd="insert into stop_times values('" + tmp.trip_id + "','" + tmp.arrival_time + "','" + tmp.departure_time + "'," + tmp.stop_id + "," + tmp.stop_sequence + ",'" + tmp.stop_headsign + "','" + tmp.pickup_type + "','" + tmp.drop_off_type + "','" + tmp.shape_dist_traveled + "')" ; 
    			try{
    				java.sql.Statement tmpstat = conn.createStatement();
    				tmpstat.execute(sqlcmd);
    				conn.commit();
    				tmpstat.close();
    			}catch(Exception ex){System.out.println(ex.toString());}
    		}
    	}
    }
    
    public static List<stops> selectFromStops(){
    	//kreiramo povratnu listu
    	List<stops> out = new ArrayList<stops>();   	
    	try{
    		//kreiramo novi statement
    		java.sql.Statement stm = conn.createStatement();
    		//0 znaci da vraca cijelu tabelu
    		stm.setFetchSize(0);
    		//kreiramo ResultSet od datom query-a
    		java.sql.ResultSet rez = stm.executeQuery("select * from stops");
    		//kreiramo metadatu od tabele (da mozemo vidjeti koliko kolona imamo)
    		java.sql.ResultSetMetaData meta = rez.getMetaData();
    		//uzimamo broj kolona iz metadata
    		int colcount = meta.getColumnCount();    	
    		//listamo kroz resultset
    		while(rez.next()){
    			//kreiramo / resetujemo varijablu row
    			String row="";
    			//te za svaki red upisujemo kolone odvojene zarezom
    			for(int i=1;i<=colcount;i++){
    				row+=rez.getString(i) + ",";
    			}
    			//nakon toga ih citamo kao i u CSV_reader-u
    			//Samo sto ovdje nema praznih stringova (svi su "NA")
    			stops tmps = new stops();
    			String[] tmpsplit = row.split(",",-1);
    			tmps.stop_id=CSV_reader.tryParseInt(tmpsplit[0]);
    			tmps.stop_code=CSV_reader.tryParseInt(tmpsplit[1]);
    			tmps.zone_id=CSV_reader.tryParseInt(tmpsplit[2]);
    			tmps.parent_station=CSV_reader.tryParseInt(tmpsplit[3]);
    			tmps.stop_name=tmpsplit[4];
    			tmps.stop_desc=tmpsplit[5];
    			tmps.stop_url=tmpsplit[6];
    			tmps.location_type=tmpsplit[7];
    			tmps.stop_lat=CSV_reader.tryParseFloat(tmpsplit[8]);
    			tmps.stop_lon=CSV_reader.tryParseFloat(tmpsplit[9]);   	
    			//Dodajemo rezultat u output
    			out.add(tmps);
    		}		
    	}catch(Exception ex){System.out.println(ex.toString());} 	
    	//te listu vracamo nazad
    	return out;
    }
    
    public static List<stops_times> selectFromStop_times(){
    	//isto kao i gore samo za stop_times
    	List<stops_times> out = new ArrayList<stops_times>();
    	try{
    		java.sql.Statement stm = conn.createStatement();
    		stm.setFetchSize(0);
    		java.sql.ResultSet rez = stm.executeQuery("select * from stop_times");
    		java.sql.ResultSetMetaData meta = rez.getMetaData();
    		int colcount = meta.getColumnCount();    		
    		while(rez.next()){
    			String row="";
    			for(int i=1;i<=colcount;i++){
    				row+=rez.getString(i) + ",";
    			}
    			stops_times tmps = new stops_times();
    			String[] tmpsplit = row.split(",",-1);
    			tmps.trip_id=tmpsplit[0];
    			tmps.arrival_time=tmpsplit[1];
    			tmps.departure_time=tmpsplit[2];
    			tmps.stop_id=CSV_reader.tryParseInt(tmpsplit[3]);
    			tmps.stop_sequence=CSV_reader.tryParseInt(tmpsplit[4]);
    			tmps.stop_headsign=tmpsplit[5];
    			tmps.pickup_type=tmpsplit[6];
    			tmps.drop_off_type=tmpsplit[7];
    			tmps.shape_dist_traveled=tmpsplit[8];
    			out.add(tmps);
    		}
    	}catch(Exception ex){System.out.println(ex.toString());}   	
    	return out;
    }
    
}
